Prensentasi

1.Git.
2.Laravel $ Lumen.
3.API.
  -Oauth 2,0.
  -Api Key.
  -Laravel Passport.
  -Dingo Api->(Frectal/Transformer).
4.VueJs & Anguler2.

Penjelasan

1.GIT
  Git adalah version control system yang digunakan para developer untuk mengembangkan software secara bersama-bersama.  Git itu sebuah software, bukanlah sebuah bahasa seperti halnya HTML,CSS atau Js bukan pula sebuah konsep atau aturan baku dalam pemrograman

  Fungsi utamanya dari pada Git adalah untuk mengatur versi dari source code anda, menambahkan checkpoint ketika terjadi perubahan pada kode Anda dan tentunya akan mempermudah Anda untuk tetap mengetahui apa saja yang berubah dari source code Anda , selain itu jika anda ingin mengembalikan Project anda dari awal anda bisa mengambil source code tersebut di Git.

Cara instal git
  1.Menggunakan Software (GUI) 
  2.install menggunakan Bash/Command (CLI) , 

#Untuk GUI anda bisa menggunakan Software:
  a.GitHub for Windows or Mac
  b.SourceTree
  c.SmartGit
  b.Aurees
#Untuk yang Berbasis Command anda bisa menggunakannya pada Linux. Anda juga   bisa menggunakannya pada Windows dan MacOS , 
 -untuk linux anda bisa mengetik script berikut untuk menginstall Git :
  untuk versi debian/ubuntu:
  "apt-get install git"
  ("apt-get adalah sebuah baris perintah handal-alat yang digunakan untuk   bekerja  dengan Ubuntu's Advanced Packaging Tool (APT) melakukan fungsi-fungsi   tersebut  sebagai instalasi paket perangkat lunak yang baru, meng-upgrade   paket perangkat  lunak yang ada, meng-update daftar paket indeks, dan bahkan   meningkatkan  seluruh sistem Ubuntu").
 -untuk windows anda perlu mendownload terlebih dahulu software Git.

2.Laravel & Lumen.
  A.Laravel
     Laravel adalah web application framework berbasis PHP yang open source, menggunakan konsep model�view�controller (MVC). Laravel berada dibawah lisensi MIT License, dengan menggunakan GitHub sebagai tempat berbagi kode
     MVC adalah sebuah pendekatan perangkat lunak yang memisahkan aplikasi   logika dari presentasi. MVC memisahkan aplikasi berdasarkan komponen- komponen   aplikasi, seperti : manipulasi data, controller, dan user interface.

   -Model, 
  Model mewakili struktur data. Biasanya model berisi fungsi-fungsi     yang   membantu seseorang dalam pengelolaan basis data seperti memasukkan data     ke   basis data, pembaruan data dan lain-lain.
   -View, 
  View adalah bagian yang mengatur tampilan ke pengguna. Bisa dikatakan      berupa halaman web.
    -Controller, 
  Controller merupakan bagian yang menjembatani model dan view.

  Anda harus memastikan server Anda memenuhi persyaratan berikut:

    PHP> = 7.1.3
    Ekstensi PHP OpenSSL
    PDO PHP Extension
    Ekstensi PHP Mbstring
    Tokenizer PHP Extension
    XML PHP Extension
    Ekstensi PHP Ctype
    JSON PHP Extension

  Memasang Laravel

Laravel menggunakan composer untuk mengelola ketergantungannya. Jadi, sebelum menggunakan Laravel, pastikan Anda telah menginstal composer pada mesin Anda.
Melalui Laravel Installer

Pertama, download installer Laravel menggunakan Composer:

--composer global require "laravel/installer"--

Pastikan untuk menempatkan direktori bin vendor seluruh vendor komposer di $ PATH Anda sehingga laravel dapat dieksekusi oleh sistem Anda. Direktori ini ada di lokasi yang berbeda berdasarkan sistem operasi Anda; Namun, beberapa lokasi yang umum termasuk:

    macOS 		      : $HOME/.composer/vendor/bin
    GNU / Linux Distributions : $HOME/.config/composer/vendor/bin

Setelah terinstal, perintah baru laravel akan membuat instalasi Laravel baru di direktori yang Anda tentukan. Misalnya, blog baru laravel akan membuat sebuah direktori bernama blog yang berisi instalasi Laravel baru dengan semua dependensi Laravel yang sudah terpasang:

--laravel new <nama dari direktori>--

Melalui Composer Create-Project

Sebagai alternatif, Anda juga dapat menginstal Laravel dengan mengeluarkan perintah create-project Komposer di terminal Anda:

--composer create-project --prefer-dist laravel/laravel <nama dari direktori>--

Server Pengembangan Lokal

Jika Anda menginstal PHP secara lokal dan Anda ingin menggunakan server pengembangan bawaan PHP untuk melayani aplikasi Anda, Anda dapat menggunakan perintah Artisan servis. Perintah ini akan memulai server pengembangan di http: // localhost: 8000:

php tukang melayanipengembangan bawaan PHP untuk melayani aplikasi Anda, Anda dapat menggunakan perintah Artisan servis. Perintah ini akan memulai server pengembangan di http: // localhost: 8000:

--php artisan serve=8000--

 B.Lumen
       Lumen adalah sebuah Micro Framework yang dirancang khusus untuk kebutuhan Micro�s Services dan juga kebutuhan API, boleh dikatakan sebagai versi mikro-nya dari Laravel
  cara install Lumen sama dengan menginstall Laravel:
Lumen # Instalasi

Untuk menginstall framework ini, ada beberapa Requirement yang dibutuhkan :

    PHP >= 5.5.9
    OpenSSL PHP Extension
    Mbstring PHP Extension
    Tokenizer PHP Extension
cara install: 
Via Lumen Installer

Pertama perlu diperhatikan bahwa metode ini saya gunakan di Environment Linux, saya tidak pernah mencobanya di Sistem Operasi Windows. Untuk melakukan instalasi dengan menggunakan Lumen Installer atau juga  disebut dengan Global Installer, kamu membutuhkan Composer, setelah itu kamu cukup mengetik perintah berikut :

    composer global require �laravel/lumen-installer=~1.0�

Setelah itu, pastikan bahwa direktori ~/.composer/vendor/bin sudah diset sebagai PATH agar kamu dapat langsung mengeksekusi perintah Lumen dari console. Setelah itu kamu dapat membuat sebuah project baru dengan mengetikan perintah berikut pada console :

     lumen new <nama direktori>

Via Composer Create-Project

Nah, ini adalah cara alternatif yang dapat kamu gunakan jika kamu tidak dapat melakukan instalasi dengan metode yang pertama. Dengan composer yang sudah terinstall pada sistem kamu, ketikkan perintah berikut pada console / command prompt milik kamu :

    composer create-project laravel/lumen �prefer-dist

perbedaan dari laravel dan lumen adalah jika laravel adalah versi full versionnya sedangkan lumen adalah micro-nya dari Laravel.

3.API
  API (Application Programming Interface)
Antarmuka pemrograman aplikasi (Application Programming Interface/API) adalah sekumpulan perintah, fungsi, dan protocol yang dapat digunakan oleh programmer saat membangun perangkat lunak untuk system operasi tertentu. API memungkinkan programmer untuk menggunakan fungsi standar untuk berinteraksi dengan system operasi.

API dapat menjelaskan cara sebuah tugas (task) tertentu dilakukan. Dalam pemrograman procedural seperti bahasa C, aksi biasanya dilakukan dengan media pemanggilan fungsi. Karena itu, API biasanya menyertakan penjelasan dari fungsi/rutin yang disediakannya.

  Cara Memakai API :
� Dilakukan dengan mengimpor package/kelas import java.util.Stack;
� Ada beberapa kelas bernama sama dipackage yang berbeda, yaitu:
- import salah satu dan gunakan nama lengkap untuk yang lain, atau gunakan nama lengkap semuanya
ada beberapa api:
A.Oauth 2.0
   sebelum saya menjaleskan tentang Oauth 2.0 saya akan menjelaskan tentang Oauth
-OAuth (Open Authorization) itu adalah Suatu protokol terbuka yang memungkinkan pengguna untuk berbagi sumber pribadi mereka (mis. foto, video, daftar alamat) yang disimpan di suatu situs web dengan situs lain tanpa perlu menyerahkan nama pengguna dan kata sandi mereka.

-OAuth 2.0 
   OAuth 2.0 adalah protokol standar industri untuk otorisasi. OAuth 2.0 menggantikan pekerjaan yang dilakukan pada protokol OAuth asli yang dibuat pada tahun 2006. OAuth 2.0 berfokus pada kesederhanaan pengembang klien sambil memberikan arus otorisasi khusus untuk aplikasi web, aplikasi desktop, telepon genggam, dan perangkat ruang tamu. Spesifikasi dan ekstensi ini dikembangkan dalam Kelompok Kerja OAuth IETF .

B.Api Key
   Kunci antarmuka pemrograman aplikasi (API key) adalah kode yang dilewatkan oleh program komputer yang memanggil antarmuka pemrograman aplikasi (API) untuk mengidentifikasi program pemanggil, pengembangnya, atau penggunanya ke situs Web. Kunci API digunakan untuk melacak dan mengontrol bagaimana API digunakan, misalnya untuk mencegah penyalahgunaan atau penyalahgunaan API (sebagaimana didefinisikan mungkin oleh persyaratan layanan).

C.Laravel Passport
    C. Laravel sudah mempermudah melakukan otentikasi melalui form login tradisional, tapi bagaimana dengan API? API biasanya menggunakan token untuk mengautentikasi pengguna dan tidak mempertahankan status sesi di antara permintaan. Laravel membuat otentikasi API mudah dengan menggunakan Paspor Paspor, yang menyediakan implementasi server OAuth2 penuh untuk aplikasi Laravel Anda dalam hitungan menit.

Installasi
Pertama tama kita harus menginstatal package passport  pada paket composer
composer require laravel/passport
selanjutnya anda harus memigrasi database
php artisan migrate
berikutnya install passport
php artisan passport: install

D.Dingo Api
    -API Dingo sejauh ini adalah paket API yang paling kuat.
API Dingo memiliki built-in authentication, error handling, responses, pagination, rate limiting, transformer dan masih banyak lagi. Pada bagian pertama ini saya akan mengarahkan Anda melalui instalasi dan konfigurasi API Dingo.

Installasi
Saya akan menarik paket Dingo via composer tapi sebelum saya dapat melakukan itu, saya perlu menetapkan stabilitas minimal untuk pengembangan di composer.json, seperti ini:

"minimum-stability": "dev"

Sekarang saya akan menarik paketnya:
Composer require dingo/api:1.0.x@dev


Konfigurasi
Ketika komposer selesai dengan instalasi, saya buka folder config / app.php dan menambahkan penyedia layanan baru ke jajaran penyedia.

Dingo\Api\Provider\LaravelServiceProvider::class

Dingo memerlukan beberapa variabel env untuk dikonfigurasi, saya akan menambahkan variabel berikut ke .env:

API_STANDARDS_TREE = vnd
API_SUBTYPE = dingo-api-guide
API_DOMAIN = dingo-api-definitive-guide.dev
API_VERSION = v1
API_NAME = "Panduan Definitif API Dingo"

di dalam api dingo ada juga ya nama nya fractal dan transformer
-fractal
Fraktal adalah lapisan transformasi default yang digunakan oleh paket. Ini menawarkan sejumlah fitur berguna untuk membantu Anda menjaga agar data tetap konsisten.
  Fraktal adalah layanan yang menguji email HTML pada klien email utama dan secara otomatis memperbaiki masalah umum yang memblokir email HTML atau membuat email HTML tidak terbaca. Uji fraktal dan memvalidasi email HTML yang memiliki properti CSS di 24 klien email dan memperbaiki kebiasaan yang diketahui di klien email. API Fraktal memungkinkan pengembang mengintegrasikan fungsionalitas Fractal ke aplikasi lain. Beberapa contoh metode API termasuk memvalidasi email HTML, memperbaiki bug dan kebiasaan di email HTML, dan memeriksa tautan di email HTML.

-transformer
Transformer memungkinkan Anda mengubah objek dengan mudah dan konsisten menjadi sebuah array. Dengan menggunakan transformator Anda bisa mengetikkan bilangan bulat dan boolean, termasuk hasil pagination, dan hubungan sarang.
Terminologi

Kata "transformator" digunakan cukup sedikit dalam bab ini. Perlu dicatat apa arti istilah berikut bila digunakan sepanjang bab ini.

    Lapisan transformasi adalah perpustakaan yang mempersiapkan dan menangani transformer.
    Trafo adalah kelas yang akan mengambil data mentah dan mengembalikannya sebagai rangkaian yang siap untuk dipformat. Cara transformator ditangani bergantung pada lapisan transformasi. 

Menggunakan Transformers

Ada beberapa cara untuk memanfaatkan transformer.
Daftarkan Transformer A untuk Kelas A

Ketika Anda mendaftarkan sebuah transformator untuk kelas tertentu, Anda akan bisa mengembalikan kelas (dengan asumsi itu bisa diubah menjadi sebuah array) langsung dari rute Anda dan akan dijalankan melalui transformator secara otomatis. Ini bagus untuk API sederhana tempat Anda menggunakan model karena Anda bisa mengembalikan model dari rute Anda.

  app ( ' Dingo \ Api \ Transformer \ Factory ' ) -> register ( ' User ' , ' UserTransformer ' ); 

4.VueJs & Anguler
   A.VueJs
      VueJs adalah suatu librari Javascript yang digunakan untuk membangun antar muka sebuah website yang interaktif. Supaya lebih singkat, untuk selanjutnya Vue.js saya tulis Vue. Library dari Vue difokuskan hanya pada view layer dan sangat mudah diimplementasikan dan diintegrasikan dengan library lain ataupun juga dengan project yang sudah ada sebelumnya. Untuk mempelajari Vue ini setidaknya kita sudah mengerti dasar-dasar HTML, CSS dan juga Javascript.
   B.AngularJs
      Angular adalah sebuah framework javascript yang memungkinkan kita membuat reaktif Single Page Aplication (SPA). Single Page Aplication yaitu aplikasi yang berjalan hanya pada satu halaman, tidak membutuhkan reload page meskipun nampak di url berpindah halaman.

Terdapat perbedaan versi yang sangat menjurang di angular, yaitu versi Angular 1 atau AngularJS, Angular 2 dan Angular 4 atau Angular (tidak menggunakan JS).
AngularJS

adalah versi angular pertama yang juga popular karena menjadi bagian dalam pembuatan web apps.
Angular 2

Angular 2 adalah versi angular yang dibuat ulang dari angularJS. Karenanya struktur dan komponen yang terdapat di dalamnya juga berbeda. Di Angular versi ini, kita juga menggunakan Typescript sebagai bahasa pemrogramannya. Typescript yaitu bahasa superset dari javascript yang sudah dicompile.

Di Angular tidak memiliki persamaan dengan Angular 1 / AngularJS jadi jika belajar AngularJS akan sangat berbeda jika ingin belajar ke Angular. Untuk itu jika belum terlanjur belajar AngularJS maka sekalian belajar Angular, Karena pembaruan versi Angular nantinya akan menggunakan versi Angular.
Angular 4

Sedangkan Angular 4 kelanjutan dari Angular 2 yang masih dalam satu keluarga. Pembaruan di versi 4 ini dari versi 2 tidak merubah total seperti saat Angular 1 ke Angular 2. Karena itu untuk Angular 2 dan Angular 4 sering hanya disebut Angular, selain itu pembaruan kedepannya juga menggunakan konsep Angular ini.
